package com.geolocation.app.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Brayo on 6/26/2015.
 */
public class GCMStore {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "gcm_id";

    public GCMStore(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createNewId(String id_name) {
        editor.putString("gcm_id", id_name);
        editor.commit();
    }

    public String[] getIds(){
        String[] tags = pref.getAll().keySet().toArray(new String[0]);
        Arrays.sort(tags);
        return tags;
    }

    public String getId(){
        String id_name = pref.getString("gcm_id", null);
        return id_name;
    }

    public ArrayList<String> getidsArraylist(){
        ArrayList<String> temp = new ArrayList<>();
        String[] ids = getIds();

        for(int i = 0; i < ids.length; i++){
            temp.add(ids[i]);
        }
        return temp;
    }
}
