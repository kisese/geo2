package com.geolocation.app.gcm;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Brayo on 2/19/2016.
 */
public class UpdateFromServer {

    String device_id, latitude, longitude, updated_at, device_name;
    Context context;

    public UpdateFromServer(Context context, String updated_at) {
        this.updated_at = updated_at;
        this.context = context;
        update();
    }

    public void update() {
        Thread t = new Thread() {

            public void run() {
                Looper.prepare(); //For Preparing Message Pool for the child Thread
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject json = new JSONObject();

                // Print on LogCat to debugin purpose
                Log.e("Personal message", "Receing data frm server");

                try {
                    HttpPost post = new HttpPost(Config.RECEIVE_URL);
                    json.put("updated_at", updated_at);

                    StringEntity se = new StringEntity(json.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

	                    /*Checking response */
                    if (response != null) {
                        InputStream in = response.getEntity().getContent(); //Get the data in the entity
                        String result = slurp(in);

                        // Print on LogCat to debugin purpose
                        Log.e("Personal message", result);

                        // Conver String to Json

                        JSONObject reader = new JSONObject(result);

                        // Print on LogCat to debugin purpose
                        Log.e("+++++=====Location message", reader.getString("message"));

                        // Execute the callback with the result from server
                        if (reader.getInt("success") == 1)
                            respond(1);
                        else
                            respond(2);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    // createDialog("Error", "Cannot Estabilish Connection");
                }

                Looper.loop(); //Loop in the message queue
            }
        };

        t.start();
    }

    public static String slurp(final InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder out = new StringBuilder();
        String newLine = System.getProperty("line.separator");
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
            out.append(newLine);
        }
        return out.toString();
    }

    public void respond(int callBack) {
        if (callBack == 1) {
            Toast.makeText(context, "Update Successful", Toast.LENGTH_LONG).show();
        }  else {
            Toast.makeText(context, "Error, Retrying...", Toast.LENGTH_LONG).show();
        }
    }
}
