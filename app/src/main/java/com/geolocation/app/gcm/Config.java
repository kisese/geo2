package com.geolocation.app.gcm;

public interface Config {

    // used to share GCM regId with application server - using php app server
    static final String REGISTER_URL = "http://kisese.hostoi.com/geo/register_user.php";
    static final String UPDATE_URL = "http://kisese.hostoi.com/geo/update_user.php";
    static final String RECEIVE_URL = "http://kisese.hostoi.com/geo/get_all_locations.php";

    // Google Project Number
    static final String GOOGLE_PROJECT_ID = "46271913143";
    static final String MESSAGE_KEY = "message";

}
